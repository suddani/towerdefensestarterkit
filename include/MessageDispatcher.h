#ifndef MESSAGEDISPATCHER_H
#define MESSAGEDISPATCHER_H

#include <list>
#include <string>

struct Message
{
    virtual ~Message(void){}
};

struct MessageDispatcher;
struct Subscriber
{
    Subscriber(void) {}
    virtual ~Subscriber(void)
    {
        std::list<regs*>::iterator it = MessageSignals.begin();
        while (it != MessageSignals.end())
        {
            regs* r = *it;
            delete r;
            ++it;
        }
        MessageSignals.clear();
    }
    virtual void receiveMessage(Message* message) = 0;
private:
    friend struct MessageDispatcher;
    struct regs
    {
        regs(void)
        {
            List = NULL;
        }
        regs(std::list<Subscriber*>* list, std::list<Subscriber*>::iterator it)
        {
            List = list;
            It = it;
        }
        regs(const regs& r)
        {
            List = r.List;
            It = r.It;
        }
        ~regs(void)
        {
            if (List)
            {
                List->erase(It);
            }
            List = NULL;
        }
        std::list<Subscriber*>* List;
        std::list<Subscriber*>::iterator It;
    };
    std::list<regs*> MessageSignals;
    void addSignal(std::list<Subscriber*>* list)
    {
        std::list<regs*>::iterator it = MessageSignals.begin();
        while (it != MessageSignals.end())
        {
            if ((*it)->List == list)
                return;
            ++it;
        }
        list->push_front(this);
        std::list<Subscriber*>::iterator It = list->begin();
        MessageSignals.push_back(new regs(list, It));
        return;
    }
    void removeSignal(std::list<Subscriber*>* list)
    {
        std::list<regs*>::iterator it = MessageSignals.begin();
        while (it != MessageSignals.end())
        {
            if ((*it)->List == list)
            {
                regs* r = *it;
                delete r;
                MessageSignals.erase(it);
                return;
            }
            ++it;
        }
        return;
    }
};

struct MessageDispatcher
{
    ~MessageDispatcher(void)
    {
        //std::list<Subscriber*>::iterator it = Listener.begin();
        while (Listener.begin() != Listener.end())
        {
            (*Listener.begin())->removeSignal(&Listener);
        }
        Listener.clear();
    }
    void Bind(Subscriber* l)
    {
        l->addSignal(&Listener);
    }
    void unBind(Subscriber* l)
    {
        l->removeSignal(&Listener);
    }
    void send(Message* message)
    {
        std::list<Subscriber*>::iterator it = Listener.begin();
        while (it != Listener.end())
        {
            (*it)->receiveMessage(message);
            ++it;
        }
    }
private:
    std::list<Subscriber*> Listener;
};

///premade templates
template<class A>
struct Subscriber_1 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
};
template<class A, class B>
struct Subscriber_2 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
};
template<class A, class B, class C>
struct Subscriber_3 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
};
template<class A, class B, class C, class D>
struct Subscriber_4 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (dynamic_cast<D*>(message))
            receiveMessage(dynamic_cast<D*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
};
template<class A, class B, class C, class D, class E>
struct Subscriber_5 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (dynamic_cast<D*>(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (dynamic_cast<E*>(message))
            receiveMessage(dynamic_cast<E*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
};
template<class A, class B, class C, class D, class E, class F>
struct Subscriber_6 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (dynamic_cast<D*>(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (dynamic_cast<E*>(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (dynamic_cast<F*>(message))
            receiveMessage(dynamic_cast<F*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G>
struct Subscriber_7 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (dynamic_cast<D*>(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (dynamic_cast<E*>(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (dynamic_cast<F*>(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (dynamic_cast<G*>(message))
            receiveMessage(dynamic_cast<G*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G, class H>
struct Subscriber_8 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (dynamic_cast<D*>(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (dynamic_cast<E*>(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (dynamic_cast<F*>(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (dynamic_cast<G*>(message))
            receiveMessage(dynamic_cast<G*>(message));
        else if (dynamic_cast<H*>(message))
            receiveMessage(dynamic_cast<H*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
    virtual void receiveMessage(H* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G, class H, class I>
struct Subscriber_9 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (dynamic_cast<D*>(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (dynamic_cast<E*>(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (dynamic_cast<F*>(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (dynamic_cast<G*>(message))
            receiveMessage(dynamic_cast<G*>(message));
        else if (dynamic_cast<H*>(message))
            receiveMessage(dynamic_cast<H*>(message));
        else if (dynamic_cast<I*>(message))
            receiveMessage(dynamic_cast<I*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
    virtual void receiveMessage(H* message) = 0;
    virtual void receiveMessage(I* message) = 0;
};
template<class A, class B, class C, class D, class E, class F, class G, class H, class I, class J>
struct Subscriber_10 : Subscriber
{
    void receiveMessage(Message* message)
    {
        if (dynamic_cast<A*>(message))
            receiveMessage(dynamic_cast<A*>(message));
        else if (dynamic_cast<B*>(message))
            receiveMessage(dynamic_cast<B*>(message));
        else if (dynamic_cast<C*>(message))
            receiveMessage(dynamic_cast<C*>(message));
        else if (dynamic_cast<D*>(message))
            receiveMessage(dynamic_cast<D*>(message));
        else if (dynamic_cast<E*>(message))
            receiveMessage(dynamic_cast<E*>(message));
        else if (dynamic_cast<F*>(message))
            receiveMessage(dynamic_cast<F*>(message));
        else if (dynamic_cast<G*>(message))
            receiveMessage(dynamic_cast<G*>(message));
        else if (dynamic_cast<H*>(message))
            receiveMessage(dynamic_cast<H*>(message));
        else if (dynamic_cast<I*>(message))
            receiveMessage(dynamic_cast<I*>(message));
        else if (dynamic_cast<J*>(message))
            receiveMessage(dynamic_cast<J*>(message));
    }
    virtual void receiveMessage(A* message) = 0;
    virtual void receiveMessage(B* message) = 0;
    virtual void receiveMessage(C* message) = 0;
    virtual void receiveMessage(D* message) = 0;
    virtual void receiveMessage(E* message) = 0;
    virtual void receiveMessage(F* message) = 0;
    virtual void receiveMessage(G* message) = 0;
    virtual void receiveMessage(H* message) = 0;
    virtual void receiveMessage(I* message) = 0;
    virtual void receiveMessage(J* message) = 0;
};

#endif // MESSAGEDISPATCHER_H
