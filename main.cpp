#include "CApplication.h"
#include "GameVariables.h"

#include "MessageDispatcher.h"

class Entity : public MessageDispatcher
{
public:
    virtual ~Entity(void){}
};
struct DamageMessage : Message
{
    int Damage;
    int Type;
};
class Player : public Entity, public Subscriber_1<DamageMessage>
{
public:
    Player(void)
    {
        this->Bind(this);
    }
    void receiveMessage(DamageMessage* message)
    {
        printf("Received %i damage of Type %i\n", message->Damage, message->Type);
    }
};


struct MyMessage : Message
{
public:
    std::string Name;
};


struct MyMessage2 : Message
{
public:
    int Health;
};

struct Test : Subscriber_2<MyMessage, MyMessage2>
{
    void receiveMessage(MyMessage* message)
    {
        printf("Received Message from \"%s\"\n", message->Name.c_str());
    }
    void receiveMessage(MyMessage2* message)
    {
        printf("Received HealthMessage with \"%i\" Health\n", message->Health);
    }
};


int main(void)
{
    Entity* p = new Player();
    DamageMessage damage;
    damage.Damage = 20;
    damage.Type = 1;
    p->send(&damage);
    delete p;



    Test* test = new Test;
    MessageDispatcher signal;
    signal.Bind(test);

    Message* message;

    MyMessage my;
    my.Name = "Bob";
    message = &my;
    signal.send(message);

    MyMessage2 my2;
    my2.Health = 432;
    message = &my2;
    signal.send(message);


    CApplication app;
    app.init(game_width*tile_size,game_height*tile_size);
    app.run();

    return 0;
}
/*
struct Module
{
    virtual ~Module(void)
    {
    }
    virtual void Initialize()=0;
    virtual Module* ExecuteModule(float timediff)=0;
    virtual void Shutdown()=0;
};
struct MainMenu : Module
{
    void Initialize()
    {
        printf("Loading images and meshes...\n");
        printf("Adding scenenodes and gui elements(images), storing pointers to them...\n");
        StartGame = false;
        shutdown = false;
    }
    Module* ExecuteModule(float timediff)
    {
        if (StartGame) //well if game is startet...
        {
            //return instance to game module
        }
        else if (shutdown) //if u want to close the game
        {
            return NULL;
        }
        return this;
    }
    void Shutdown()
    {
        printf("Unload scenenodes and guielements from stored pointers..\n");
    }
    bool StartGame;
    bool shutdown;
};
struct Spalshscreen : Module
{
    void Initialize()
    {
        printf("Loading images and meshes...\n");
        printf("Adding scenenodes and gui elements(images), storing pointers to them...\n");
        counter = 0;
    }
    Module* ExecuteModule(float timediff)
    {
        counter+=timediff;
        if (counter > 10.f) //after 10 seconds
        {
            return new MainMenu;
        }
        return this;
    }
    void Shutdown()
    {
        printf("Unload scenenodes and guielements from stored pointers..\n");
    }
    float counter;
};
int main(void)
{
    Module* gamemod = new Spalshscreen();
    gamemod->Initialize();

    while (gamemod)
    {
        Module* oldMod = gamemod;
        if ((gamemod=gamemod->ExecuteModule(0.01f))!=oldMod)
        {
            oldMod->Shutdown();
            delete oldMod;
            if (gamemod)
                gamemod->Initialize();
        }

    }

    return 0;
}
*/
